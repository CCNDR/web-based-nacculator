
from django.conf import settings
from django.urls import include, path
#from rest_framework import routers, serializers, viewsets
from django.contrib import admin
from django.views.generic import TemplateView
#from apps.home import views
#import apps
from django.conf.urls.static import static

from . import views

from django.conf.urls import url


urlpatterns = [

    path('', views.redcaptonacc, name='redcaptonacc'),
    path('download/<slug:folder>/<slug:filename>', views.download, name='download'),
    

    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
