from django.shortcuts import render
from django.shortcuts import redirect
from naccweb import settings
from django.contrib import messages
from django.urls import reverse
from .forms import *
import uuid
import os
from input_sanitizer import sanitizers
# Create your views here.

from io import BytesIO, StringIO

from shutil import make_archive
from wsgiref.util import FileWrapper

from django.http import HttpResponseRedirect, HttpResponse
import shutil
import subprocess
import re


def handle_uploaded_file(f, path):
	with open(path, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)

def redcaptonacc(request):
	
	# if not request.user.is_authenticated:
	#     return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
	# else:

	form = AssessFileForm(request.POST or None, request.FILES or None)

	if sanitizers.sanitize_data(request.POST.get("submit")):

		if form.is_valid():			
			bad_characters = ["{", "}", "", "(", ")", "<", ">", "&", "*", "|", "=", "?", ";", "[", "]", "$", "~", "!", ".", "%", "/",":", "+", "'",'"\"']
			source = request.FILES['importFile']
			
			for i in bad_characters:
				if i in source:
					source = source.replace(i, '')

			fname, filetype = os.path.splitext(sanitizers.sanitize_data(source.name))
			if not filetype.lower() == '.csv':
				# return redirect(request.META.get('HTTP_REFERER'))
				return redirect('redcaptonacc')

			for i in bad_characters:
				if i in fname:
					fname = fname.replace(i, '')
				if i in filetype:
					filetype = filetype.replace(i, '')
				
				
			fname = re.sub(r"\s+", "", fname)
			# Regular expression pattern to match binary representation
			binary_pattern = r'[01]{8}'
			# Use re.sub() to remove binary representation from the string
			fname = re.sub(binary_pattern, '', fname)
			
			filetype = re.sub(r"\s+", "", filetype)
			# Regular expression pattern to match binary representation
			binary_pattern = r'[01]{8}'
			# Use re.sub() to remove binary representation from the string
			filetype = re.sub(binary_pattern, '', filetype)
			

			
			if not filetype:
				messages.error(request, 'invalid file type.')
				return HttpResponseRedirect(reverse('redcaptonacc'), {})
			ext = str(filetype).lower()
			fileName = str(uuid.uuid4()) + str(ext)
			folder = "nacc_"+ datetime.datetime.now().strftime('%Y-%m-%d') + str(uuid.uuid4())
			os.mkdir(os.path.join(settings.MEDIA_ROOT, 'uploads/'+folder))

			filePath = os.path.join(settings.MEDIA_ROOT, 'uploads/'+folder)

			# adding filter file upload option here

			sourcefilter = request.FILES['filterFile'] if 'filterFile' in request.FILES else False
			if sourcefilter:
				for i in bad_characters:
					if i in sourcefilter:
						sourcefilter = sourcefilter.replace(i, '')
				
				if sourcefilter!=False:
					filname, filfiletype = os.path.splitext(sanitizers.sanitize_data(sourcefilter.name))
					if not filfiletype.lower() == '.txt':
						# return redirect(request.META.get('HTTP_REFERER'))
						return redirect('redcaptonacc')
					filname = re.sub(r"\s+", "", filname)
					# Regular expression pattern to match binary representation
					binary_pattern = r'[01]{8}'
					# Use re.sub() to remove binary representation from the string
					filname = re.sub(binary_pattern, '', filname)
					
					filfiletype = re.sub(r"\s+", "", filfiletype)
					# Regular expression pattern to match binary representation
					binary_pattern = r'[01]{8}'
					# Use re.sub() to remove binary representation from the string
					filfiletype = re.sub(binary_pattern, '', filfiletype)
					
					
					if not filfiletype:
						messages.error(request, 'invalid file type.')
						return HttpResponseRedirect(reverse('redcaptonacc'), {})
					filext = str(filfiletype).lower()
					filfileName = str(uuid.uuid4()) + str(filext)
					handle_uploaded_file(sourcefilter, os.path.join(filePath, filfileName))

			# end Filter File conversion

			handle_uploaded_file(source, os.path.join(filePath, fileName))
			filtercsv_arr = ['custom','yes','no']
			filterdrugid_arr = ['yes','no']
			cleancsv = sanitizers.sanitize_data(request.POST.get("filtercsv")) if request.POST.get("filtercsv") in filtercsv_arr else ''
			fdid = sanitizers.sanitize_data(request.POST.get("filterdrugid")) if request.POST.get("filterdrugid") in filterdrugid_arr else ''

			#Execute redcap command to convert data
			if sourcefilter!=False:
				filter_file_path = os.path.join(filePath, filfileName)
			csv_file_path = os.path.join(filePath, fileName)
			txt_file = fname+ '.txt'
			txt_file_path = os.path.join(filePath, txt_file)
			clean_file = fname+ '_clean.csv'
			clean_file_path = os.path.join(filePath, clean_file)
			fdid_file = fname+ '_fdid.csv'
			fdid_file_path = os.path.join(filePath, fdid_file)

			errfname = fname+ '_log.txt'
			err_file_path = os.path.join(filePath, errfname)
			filter_arr = ["ivp","fvp","tfp","tfp3","np","m","csf","lbd","lbd -ivp","lbd -fvp","lbdsv","lbdsv -ivp","lbdsv -fvp","ftld","ftld -ivp","ftld -fvp"]
			filtertype = sanitizers.sanitize_data(request.POST.get("filterby")) if request.POST.get("filterby") in filter_arr else ''
			
			for i in bad_characters:
				if i in filtertype:
					filtertype = filtertype.replace(i, '')
			# if filtertype == "lbd" or filtertype == "lbdsv":
			# 	subprocess.check_output('redcap2nacc -'+filtertype+' -ivp -file  ' +csv_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)
			# else:
			# 	subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +csv_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)
			try:
				if cleancsv == "yes" and fdid== "yes":
					subprocess.check_output('redcap2nacc -f fixHeaders -meta filter.txt -file ' +csv_file_path +' 2>> '+err_file_path + '> '+clean_file_path, shell=True)
					subprocess.check_output('redcap2nacc --filter replaceDrugId -meta drugid.txt -file ' +clean_file_path +' 2>> '+err_file_path + '> '+fdid_file_path, shell=True)
					# subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +fdid_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)
					
					command = ['redcap2nacc','-' + filtertype,'-file',fdid_file_path,]					
					with open(err_file_path, 'a') as err_file, open(txt_file_path, 'w') as txt_file:
						subprocess.check_output(command, stderr=err_file, stdout=txt_file)

				elif cleancsv == "yes" and fdid== "no":
					subprocess.check_output('redcap2nacc -f fixHeaders -meta filter.txt -file ' +csv_file_path +' 2>> '+err_file_path + '> '+clean_file_path, shell=True)
					# subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +clean_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)
					
					command = ['redcap2nacc', f'-{filtertype}', '-file', clean_file_path]					
					with open(txt_file_path, 'w') as txt_file, open(err_file_path, 'a') as err_file:						
						subprocess.run(command, stdout=txt_file, stderr=err_file)

				elif cleancsv == "no" and fdid== "yes":
					subprocess.check_output('redcap2nacc --filter replaceDrugId -meta drugid.txt -file ' +csv_file_path +' 2>> '+err_file_path + '> '+fdid_file_path, shell=True)
					# subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +fdid_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)
					
					command = ['redcap2nacc', f'-{filtertype}', '-file', fdid_file_path]					
					with open(txt_file_path, 'w') as txt_file, open(err_file_path, 'a') as err_file:						
						subprocess.run(command, stdout=txt_file, stderr=err_file)

				elif cleancsv == "custom" and fdid== "no":
					subprocess.check_output('redcap2nacc -f fixHeaders -meta ' +filter_file_path + ' -file ' +csv_file_path +' 2>> '+err_file_path + '> '+clean_file_path, shell=True)
					# subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +clean_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)
					
					command = ['redcap2nacc', f'-{filtertype}', '-file', clean_file_path]					
					with open(txt_file_path, 'w') as txt_file, open(err_file_path, 'a') as err_file:						
						subprocess.run(command, stdout=txt_file, stderr=err_file)

				elif cleancsv == "custom" and fdid== "yes":
					subprocess.check_output('redcap2nacc -f fixHeaders -meta ' +filter_file_path + ' -file ' +csv_file_path +' 2>> '+err_file_path + '> '+clean_file_path, shell=True)
					subprocess.check_output('redcap2nacc --filter replaceDrugId -meta drugid.txt -file ' +clean_file_path +' 2>> '+err_file_path + '> '+fdid_file_path, shell=True)
					# subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +fdid_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)

					command = ['redcap2nacc', f'-{filtertype}', '-file', fdid_file_path]					
					with open(txt_file_path, 'w') as txt_file, open(err_file_path, 'a') as err_file:
						try:							
							subprocess.run(command, stdout=txt_file, stderr=err_file)
						except subprocess.CalledProcessError as e:							
							print(f"An error occurred: {e}")
				
				else:
					# subprocess.check_output('redcap2nacc -'+filtertype+' -file  ' +csv_file_path +' 2>> '+err_file_path + '> '+txt_file_path, shell=True)

					command = ['redcap2nacc', f'-{filtertype}', '-file', csv_file_path]					
					with open(txt_file_path, 'w') as txt_file, open(err_file_path, 'a') as err_file:
						try:							
							subprocess.run(command, stdout=txt_file, stderr=err_file)
						except subprocess.CalledProcessError as e:							
							print(f"An error occurred: {e}")

				#delete csv file from folder
			except subprocess.CalledProcessError as e:
				print(e.returncode)

			os.remove(csv_file_path)

			#Zip converted file and then download

			#return HttpResponseRedirect(reverse('download', args=[folder,fname]), {})
			form = AssessFileForm()
			return render(request, 'redcaptonacc.html', {'assessform': form, 'filepath':fname, 'folderpath':folder})
			
			
	return render(request, 'redcaptonacc.html', {'assessform': form, 'filepath':'', 'folderpath':''})


def download(request, folder, filename):
	files_path = os.path.join(settings.MEDIA_ROOT, 'uploads/'+folder)
	file_name = filename

	path_to_zip = make_archive(files_path, "zip", files_path)
	response = HttpResponse(FileWrapper(open(path_to_zip,'rb')), content_type='application/zip')
	response['Content-Disposition'] = 'attachment; filename="{filename}.zip"'.format(
		filename = file_name.replace(" ", "_")
	)

	shutil.rmtree(files_path, ignore_errors=True)
	os.remove(files_path+'.zip')

	#return HttpResponseRedirect('')
	return response