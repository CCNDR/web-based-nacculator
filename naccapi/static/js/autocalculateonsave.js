    var url = $(location).attr('href'),
        parts = url.split("/"),
        app_name = parts[parts.length-1];

        if(url.indexOf('/admin') != -1){

            app_name = parts[parts.length-4].toUpperCase();
        }
        

    /**
    *
    *BRIEF VISUOSPATIAL MEMORY TEST, REVISED (BVMT)
    */

    if(app_name=='C345524'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C224812' || fieldId=='id_C224814' || fieldId=='id_C224816'){

                
                var id_C224812,id_C224814,id_C224816,total_raw;
                id_C224812 = $("#id_C224812").val();
                id_C224814 = $("#id_C224814").val();
                id_C224816 = $("#id_C224816").val();

                if(id_C224812==''){
                   id_C224812 = 0 
                }
                if(id_C224814==''){
                   id_C224814 = 0 
                }
                if(id_C224816==''){
                   id_C224816 = 0 
                }

                total_raw = parseInt(id_C224812)+parseInt(id_C224814)+parseInt(id_C224816);

                if(total_raw!=0){
                    $("#id_C224818").val(total_raw);
                }else{
                    $("#id_C224818").val('');
                }
              

            }
            if(fieldId=='id_C224826' || fieldId=='id_C224828'){

                
                var id_C224826,id_C224828,total_rec_raw;
                id_C224826 = $("#id_C224826").val();
                id_C224828 = $("#id_C224828").val();

                if(id_C224826==''){
                   id_C224826 = 0 
                }
                if(id_C224828==''){
                   id_C224828 = 0 
                }
                if(id_C224826!=0){
                    total_rec_raw = parseInt(id_C224826)-parseInt(id_C224828);
                    $("#id_C224830").val(total_rec_raw);
                }else{
                    $("#id_C224830").val('');
                }

            }
        });       
    }

    /**
    *
    *WECHSLER MEMORY SCALE, 4TH EDITION, LOGICAL 
    *MEMORY, OLDER ADULT (WMS-IV LM-OA)
    */
    if(app_name=='C222904'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C444930' || fieldId=='id_C444931' || fieldId=='id_C444932'){

                
                var id_C444930,id_C444931,id_C444932,total_raw;
                id_C444930 = $("#id_C444930").val();
                id_C444931 = $("#id_C444931").val();
                id_C444932 = $("#id_C444932").val();

                if(id_C444930==''){
                   id_C444930 = 0 
                }
                if(id_C444931==''){
                   id_C444931 = 0 
                }
                if(id_C444932==''){
                   id_C444932 = 0 
                }

                total_raw = parseInt(id_C444930)+parseInt(id_C444931)+parseInt(id_C444932);

                if(total_raw!=0){
                    $("#id_C444933").val(total_raw);
                }else{
                    $("#id_C444933").val('');
                }
              

            }
            if(fieldId=='id_C444935' || fieldId=='id_C444936'){

                
                var id_C444935,id_C444936,total_rec_raw;
                id_C444935 = $("#id_C444935").val();
                id_C444936 = $("#id_C444936").val();

                if(id_C444935==''){
                   id_C444935 = 0 
                }
                if(id_C444936==''){
                   id_C444936 = 0 
                }
                
                total_rec_raw = parseInt(id_C444935)+parseInt(id_C444936);

                if(total_rec_raw!=0){
                    
                    $("#id_C444937").val(total_rec_raw);
                }else{
                    $("#id_C444937").val('');
                }

            }
        });       
    }

    /**
    *
    *HOPKINS VERBAL LEARNING TEST, REVISED (HVLT)
    */
    if(app_name=='C347422'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C824993' || fieldId=='id_C824995' || fieldId=='id_C824997'){

                
                var id_C824993,id_C824995,id_C824997,total_raw;
                id_C824993 = $("#id_C824993").val();
                id_C824995 = $("#id_C824995").val();
                id_C824997 = $("#id_C824997").val();

                if(id_C824993==''){
                   id_C824993 = 0 
                }
                if(id_C824995==''){
                   id_C824995 = 0 
                }
                if(id_C824997==''){
                   id_C824997 = 0 
                }

                total_raw = parseInt(id_C824993)+parseInt(id_C824995)+parseInt(id_C824997);

                if(total_raw!=0){
                    $("#id_C824999").val(total_raw);
                }else{
                    $("#id_C824999").val('');
                }
              

            }
            if(fieldId=='id_C825005' || fieldId=='id_C825006'){

                
                var id_C825005,id_C825006,total_rec_raw;
                id_C825005 = $("#id_C825005").val();
                id_C825006 = $("#id_C825006").val();

                if(id_C825005==''){
                   id_C825005 = 0 
                }
                if(id_C825006==''){
                   id_C825006 = 0 
                }

                if(id_C825005!=0){
                    total_rec_raw = parseInt(id_C825005)-parseInt(id_C825006);
                    $("#id_C825007").val(total_rec_raw);
                }else{
                    $("#id_C825007").val('');
                }

            }
        });       
    }

    /**
    *
    *WECHSLER ADULT INTELLIGENCE SCALE, 4TH EDITION, DIGIT SPAN (WAIS-IV DS)
    */
    if(app_name=='C526375'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C593308' || fieldId=='id_C593310' || fieldId=='id_C593312'){

                
                var id_C593308,id_C593310,id_C593312,total_raw;
                id_C593308 = $("#id_C593308").val();
                id_C593310 = $("#id_C593310").val();
                id_C593312 = $("#id_C593312").val();

                if(id_C593308==''){
                   id_C593308 = 0 
                }
                if(id_C593310==''){
                   id_C593310 = 0 
                }
                if(id_C593312==''){
                   id_C593312 = 0 
                }

                total_raw = parseInt(id_C593308)+parseInt(id_C593310)+parseInt(id_C593312);

                if(total_raw!=0){
                    $("#id_C593306").val(total_raw);
                }else{
                    $("#id_C593306").val('');
                }
              

            }
           
        });       
    }



    /**
    *
    *BECK ANXIETY INVENTORY (BAI)
    */
    if(app_name=='C879287'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C617759' || fieldId=='id_C617760' 
                || fieldId=='id_C617761' || fieldId=='id_C617762' 
                || fieldId=='id_C617763' || fieldId=='id_C617764' 
                || fieldId=='id_C617765' || fieldId=='id_C617766' 
                || fieldId=='id_C617767' || fieldId=='id_C617768' 
                || fieldId=='id_C617769' || fieldId=='id_C617770' 
                || fieldId=='id_C617771' || fieldId=='id_C617772' 
                || fieldId=='id_C617773' || fieldId=='id_C617774' 
                || fieldId=='id_C617775' || fieldId=='id_C617776' 
                || fieldId=='id_C617777' || fieldId=='id_C617778' 
                || fieldId=='id_C617779'){

                
                var id_C617759,id_C617760,id_C617761,id_C617762,id_C617763,id_C617764,
            id_C617765,id_C617766,id_C617767,id_C617768,
            id_C617769,id_C617770,id_C617771,id_C617772,
            id_C617773,id_C617774,id_C617775,id_C617776,
            id_C617777,id_C617778,id_C617779,
            total_raw;
                
                id_C617759 = $("#id_C617759").val();
                id_C617760 = $("#id_C617760").val();
                id_C617761 = $("#id_C617761").val();
                id_C617762 = $("#id_C617762").val();
                id_C617763 = $("#id_C617763").val();
                id_C617764 = $("#id_C617764").val();
                id_C617765 = $("#id_C617765").val();
                id_C617766 = $("#id_C617766").val();
                id_C617767 = $("#id_C617767").val();
                id_C617768 = $("#id_C617768").val();
                id_C617769 = $("#id_C617769").val();
                id_C617770 = $("#id_C617770").val();
                id_C617771 = $("#id_C617771").val();
                id_C617772 = $("#id_C617772").val();
                id_C617773 = $("#id_C617773").val();
                id_C617774 = $("#id_C617774").val();
                id_C617775 = $("#id_C617775").val();
                id_C617776 = $("#id_C617776").val();
                id_C617777 = $("#id_C617777").val();
                id_C617778 = $("#id_C617778").val();
                id_C617779 = $("#id_C617779").val();
                

                if(id_C617759==''){
                   id_C617759 = 0 
                }
                if(id_C617760==''){
                   id_C617760 = 0 
                }
                if(id_C617761==''){
                   id_C617761 = 0 
                }
                if(id_C617762==''){
                   id_C617762 = 0 
                }
                if(id_C617763==''){
                   id_C617763 = 0 
                }
                if(id_C617764==''){
                   id_C617764 = 0 
                }
                if(id_C617765==''){
                   id_C617765 = 0 
                }
                if(id_C617766==''){
                   id_C617766 = 0 
                }
                if(id_C617767==''){
                   id_C617767 = 0 
                }
                if(id_C617768==''){
                   id_C617768 = 0 
                }
                if(id_C617769==''){
                   id_C617769 = 0 
                }
                if(id_C617770==''){
                   id_C617770 = 0 
                }
                if(id_C617771==''){
                   id_C617771 = 0 
                }
                if(id_C617772==''){
                   id_C617772 = 0 
                }
                if(id_C617773==''){
                   id_C617773 = 0 
                }
                if(id_C617774==''){
                   id_C617774 = 0 
                }
                if(id_C617775==''){
                   id_C617775 = 0 
                }
                if(id_C617776==''){
                   id_C617776 = 0 
                }
                if(id_C617777==''){
                   id_C617777 = 0 
                }
                if(id_C617778==''){
                   id_C617778 = 0 
                }
                if(id_C617779==''){
                   id_C617779 = 0 
                }
                

                total_raw = parseInt(id_C617759)+parseInt(id_C617760)+parseInt(id_C617761)+parseInt(id_C617762)+parseInt(id_C617763)+parseInt(id_C617764)+
                parseInt(id_C617765)+parseInt(id_C617766)+parseInt(id_C617767)+parseInt(id_C617768)+
                parseInt(id_C617769)+parseInt(id_C617770)+parseInt(id_C617771)+parseInt(id_C617772)+
                parseInt(id_C617773)+parseInt(id_C617774)+parseInt(id_C617775)+parseInt(id_C617776)+
                parseInt(id_C617777)+parseInt(id_C617778)+parseInt(id_C617779);

                if(total_raw!=0){
                    $("#id_C617780").val(total_raw);
                }else{
                    $("#id_C617780").val('');
                }
              

            }
           
        });       
    }



    /**
    *
    *GERIATRIC DEPRESSION SCALE (GDS)
    */
    if(app_name=='C106663'){
        
        $(document).on('change', 'select', function (event) {
            var resultval=0;
            
            $(".results-row").find('select').each(function(){
                var inputval = $(this).val();
                if(inputval!=''){
                    resultval= resultval +1;
                }
                
            });

            if(resultval>0){
                $("#id_C106794").val(resultval);

            }
           
        }); 

        $(document).on('change', 'input:checkbox', function (event) {
            var resultval=0;
            
            $(".results-row").find('select').each(function(){
                var inputval = $(this).val();
                if(inputval!=''){
                    resultval= resultval +1;
                }
                
            });

            if(resultval>0){
                $("#id_C106794").val(resultval);

            }
           
        });       
    }




    /**
    *
    *Neuropsychiatric Inventory Questionnaire (NPI-Q)
    */
    if(app_name=='C491006'){
        
        
        $(document).on('change', '#id_C979959', function (event) {
            var inputval = parseInt($(this).val());            

            $("#id_C979960").val('')
            $("#id_C979961").val('')


            if(inputval==0){
                $("input[name='optional[C979960]']").prop( "checked", true );
                $("input[name='optional[C979961]']").prop( "checked", true );
               

                $("#id_C979960").removeAttr('required')            
                $("#id_C979960").attr('disabled', true)            
                $("#id_C979960").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979960").parent().parent().find('span.red').hide()
                //$("#id_C979960").val('')


                $("#id_C979961").removeAttr('required')            
                $("#id_C979961").attr('disabled', true)            
                $("#id_C979961").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979961").parent().parent().find('span.red').hide()
                //$("#id_C979961").val('')


            }else if(inputval==1){
                $("input[name='optional[C979960]']").prop( "checked", false );
                $("input[name='optional[C979961]']").prop( "checked", false );
                
                $("#id_C979960").attr('required',true)
                $("#id_C979960").attr('disabled', false)
                $("#id_C979960").css({"color":"black"})
                $("#id_C979960").parent().parent().find('span.red').show()

                $("#id_C979961").attr('required',true)
                $("#id_C979961").attr('disabled', false)
                $("#id_C979961").css({"color":"black"})
                $("#id_C979961").parent().parent().find('span.red').show()

            }   
           
        }); 

        $(document).on('change', '#id_C979962', function (event) {
            var inputval = parseInt($(this).val());


            $("#id_C979963").val('')
            $("#id_C979964").val('')
            
            if(inputval==0){
                $("input[name='optional[C979963]']").prop( "checked", true );
                $("input[name='optional[C979964]']").prop( "checked", true );
               

                $("#id_C979963").removeAttr('required')            
                $("#id_C979963").attr('disabled', true)            
                $("#id_C979963").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979963").parent().parent().find('span.red').hide()
                //$("#id_C979963").val('')


                $("#id_C979964").removeAttr('required')            
                $("#id_C979964").attr('disabled', true)            
                $("#id_C979964").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979964").parent().parent().find('span.red').hide()
                //$("#id_C979964").val('')


            }else if(inputval==1){
                $("input[name='optional[C979963]']").prop( "checked", false );
                $("input[name='optional[C979964]']").prop( "checked", false );
                
                $("#id_C979963").attr('required',true)
                $("#id_C979963").attr('disabled', false)
                $("#id_C979963").css({"color":"black"})
                $("#id_C979963").parent().parent().find('span.red').show()

                $("#id_C979964").attr('required',true)
                $("#id_C979964").attr('disabled', false)
                $("#id_C979964").css({"color":"black"})
                $("#id_C979964").parent().parent().find('span.red').show()

            }   
           
        }); 


        $(document).on('change', '#id_C979965', function (event) {
            var inputval = parseInt($(this).val());
            

            $("#id_C979966").val('')
            $("#id_C979967").val('')

            if(inputval==0){
                $("input[name='optional[C979966]']").prop( "checked", true );
                $("input[name='optional[C979967]']").prop( "checked", true );
               

                $("#id_C979966").removeAttr('required')            
                $("#id_C979966").attr('disabled', true)            
                $("#id_C979966").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979966").parent().parent().find('span.red').hide()
                //$("#id_C979966").val('')


                $("#id_C979967").removeAttr('required')            
                $("#id_C979967").attr('disabled', true)            
                $("#id_C979967").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979967").parent().parent().find('span.red').hide()
                //$("#id_C979967").val('')


            }else if(inputval==1){
                $("input[name='optional[C979966]']").prop( "checked", false );
                $("input[name='optional[C979967]']").prop( "checked", false );
                
                $("#id_C979966").attr('required',true)
                $("#id_C979966").attr('disabled', false)
                $("#id_C979966").css({"color":"black"})
                $("#id_C979966").parent().parent().find('span.red').show()

                $("#id_C979967").attr('required',true)
                $("#id_C979967").attr('disabled', false)
                $("#id_C979967").css({"color":"black"})
                $("#id_C979967").parent().parent().find('span.red').show()

            }   
           
        });


        


        $(document).on('change', '#id_C979968', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979969").val('')
            $("#id_C979970").val('')
            
            if(inputval==0){
                $("input[name='optional[C979969]']").prop( "checked", true );
                $("input[name='optional[C979970]']").prop( "checked", true );
               

                $("#id_C979969").removeAttr('required')            
                $("#id_C979969").attr('disabled', true)            
                $("#id_C979969").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979969").parent().parent().find('span.red').hide()
                //$("#id_C979969").val('')


                $("#id_C979970").removeAttr('required')            
                $("#id_C979970").attr('disabled', true)            
                $("#id_C979970").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979970").parent().parent().find('span.red').hide()
                //$("#id_C979970").val('')


            }else if(inputval==1){
                $("input[name='optional[C979969]']").prop( "checked", false );
                $("input[name='optional[C979970]']").prop( "checked", false );
                
                $("#id_C979969").attr('required',true)
                $("#id_C979969").attr('disabled', false)
                $("#id_C979969").css({"color":"black"})
                $("#id_C979969").parent().parent().find('span.red').show()

                $("#id_C979970").attr('required',true)
                $("#id_C979970").attr('disabled', false)
                $("#id_C979970").css({"color":"black"})
                $("#id_C979970").parent().parent().find('span.red').show()

            }   
           
        });

        $(document).on('change', '#id_C979971', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979972").val('')
            $("#id_C979973").val('')

            
            if(inputval==0){
                $("input[name='optional[C979972]']").prop( "checked", true );
                $("input[name='optional[C979973]']").prop( "checked", true );
               

                $("#id_C979972").removeAttr('required')            
                $("#id_C979972").attr('disabled', true)            
                $("#id_C979972").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979972").parent().parent().find('span.red').hide()
                //$("#id_C979972").val('')


                $("#id_C979973").removeAttr('required')            
                $("#id_C979973").attr('disabled', true)            
                $("#id_C979973").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979973").parent().parent().find('span.red').hide()
                //$("#id_C979973").val('')


            }else if(inputval==1){
                $("input[name='optional[C979972]']").prop( "checked", false );
                $("input[name='optional[C979973]']").prop( "checked", false );
                
                $("#id_C979972").attr('required',true)
                $("#id_C979972").attr('disabled', false)
                $("#id_C979972").css({"color":"black"})
                $("#id_C979972").parent().parent().find('span.red').show()

                $("#id_C979973").attr('required',true)
                $("#id_C979973").attr('disabled', false)
                $("#id_C979973").css({"color":"black"})
                $("#id_C979973").parent().parent().find('span.red').show()

            }   
           
        });
        $(document).on('change', '#id_C979974', function (event) {
            var inputval = parseInt($(this).val());


            $("#id_C979975").val('')
            $("#id_C979976").val('')
            
            if(inputval==0){
                $("input[name='optional[C979975]']").prop( "checked", true );
                $("input[name='optional[C979976]']").prop( "checked", true );
               

                $("#id_C979975").removeAttr('required')            
                $("#id_C979975").attr('disabled', true)            
                $("#id_C979975").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979975").parent().parent().find('span.red').hide()
                //$("#id_C979975").val('')


                $("#id_C979976").removeAttr('required')            
                $("#id_C979976").attr('disabled', true)            
                $("#id_C979976").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979976").parent().parent().find('span.red').hide()
                //$("#id_C979976").val('')


            }else if(inputval==1){
                $("input[name='optional[C979975]']").prop( "checked", false );
                $("input[name='optional[C979976]']").prop( "checked", false );
                
                $("#id_C979975").attr('required',true)
                $("#id_C979975").attr('disabled', false)
                $("#id_C979975").css({"color":"black"})
                $("#id_C979975").parent().parent().find('span.red').show()

                $("#id_C979976").attr('required',true)
                $("#id_C979976").attr('disabled', false)
                $("#id_C979976").css({"color":"black"})
                $("#id_C979976").parent().parent().find('span.red').show()

            }   
           
        });



        $(document).on('change', '#id_C979977', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979978").val('')
            $("#id_C979979").val('')
            
            if(inputval==0){
                $("input[name='optional[C979978]']").prop( "checked", true );
                $("input[name='optional[C979979]']").prop( "checked", true );
               

                $("#id_C979978").removeAttr('required')            
                $("#id_C979978").attr('disabled', true)            
                $("#id_C979978").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979978").parent().parent().find('span.red').hide()
                //$("#id_C979978").val('')


                $("#id_C979979").removeAttr('required')            
                $("#id_C979979").attr('disabled', true)            
                $("#id_C979979").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979979").parent().parent().find('span.red').hide()
                //$("#id_C979979").val('')


            }else if(inputval==1){
                $("input[name='optional[C979978]']").prop( "checked", false );
                $("input[name='optional[C979979]']").prop( "checked", false );
                
                $("#id_C979978").attr('required',true)
                $("#id_C979978").attr('disabled', false)
                $("#id_C979978").css({"color":"black"})
                $("#id_C979978").parent().parent().find('span.red').show()

                $("#id_C979979").attr('required',true)
                $("#id_C979979").attr('disabled', false)
                $("#id_C979979").css({"color":"black"})
                $("#id_C979979").parent().parent().find('span.red').show()

            }   
           
        });



        $(document).on('change', '#id_C979980', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979981").val('')
            $("#id_C979982").val('')
            
            if(inputval==0){
                $("input[name='optional[C979981]']").prop( "checked", true );
                $("input[name='optional[C979982]']").prop( "checked", true );
               

                $("#id_C979981").removeAttr('required')            
                $("#id_C979981").attr('disabled', true)            
                $("#id_C979981").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979981").parent().parent().find('span.red').hide()
                //$("#id_C979981").val('')


                $("#id_C979982").removeAttr('required')            
                $("#id_C979982").attr('disabled', true)            
                $("#id_C979982").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979982").parent().parent().find('span.red').hide()
                //$("#id_C979982").val('')


            }else if(inputval==1){
                $("input[name='optional[C979981]']").prop( "checked", false );
                $("input[name='optional[C979982]']").prop( "checked", false );
                
                $("#id_C979981").attr('required',true)
                $("#id_C979981").attr('disabled', false)
                $("#id_C979981").css({"color":"black"})
                $("#id_C979981").parent().parent().find('span.red').show()

                $("#id_C979982").attr('required',true)
                $("#id_C979982").attr('disabled', false)
                $("#id_C979982").css({"color":"black"})
                $("#id_C979982").parent().parent().find('span.red').show()

            }   
           
        });


        $(document).on('change', '#id_C979983', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979984").val('')
            $("#id_C979985").val('')
            
            if(inputval==0){
                $("input[name='optional[C979984]']").prop( "checked", true );
                $("input[name='optional[C979985]']").prop( "checked", true );
               

                $("#id_C979984").removeAttr('required')            
                $("#id_C979984").attr('disabled', true)            
                $("#id_C979984").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979984").parent().parent().find('span.red').hide()
                //$("#id_C979984").val('')


                $("#id_C979985").removeAttr('required')            
                $("#id_C979985").attr('disabled', true)            
                $("#id_C979985").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979985").parent().parent().find('span.red').hide()
                //$("#id_C979985").val('')


            }else if(inputval==1){
                $("input[name='optional[C979984]']").prop( "checked", false );
                $("input[name='optional[C979985]']").prop( "checked", false );
                
                $("#id_C979984").attr('required',true)
                $("#id_C979984").attr('disabled', false)
                $("#id_C979984").css({"color":"black"})
                $("#id_C979984").parent().parent().find('span.red').show()

                $("#id_C979985").attr('required',true)
                $("#id_C979985").attr('disabled', false)
                $("#id_C979985").css({"color":"black"})
                $("#id_C979985").parent().parent().find('span.red').show()

            }   
           
        });


        $(document).on('change', '#id_C979986', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979987").val('')
            $("#id_C979988").val('')
            
            if(inputval==0){
                $("input[name='optional[C979987]']").prop( "checked", true );
                $("input[name='optional[C979988]']").prop( "checked", true );
               

                $("#id_C979987").removeAttr('required')            
                $("#id_C979987").attr('disabled', true)            
                $("#id_C979987").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979987").parent().parent().find('span.red').hide()
                //$("#id_C979987").val('')


                $("#id_C979988").removeAttr('required')            
                $("#id_C979988").attr('disabled', true)            
                $("#id_C979988").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979988").parent().parent().find('span.red').hide()
                //$("#id_C979988").val('')


            }else if(inputval==1){
                $("input[name='optional[C979987]']").prop( "checked", false );
                $("input[name='optional[C979988]']").prop( "checked", false );
                
                $("#id_C979987").attr('required',true)
                $("#id_C979987").attr('disabled', false)
                $("#id_C979987").css({"color":"black"})
                $("#id_C979987").parent().parent().find('span.red').show()

                $("#id_C979988").attr('required',true)
                $("#id_C979988").attr('disabled', false)
                $("#id_C979988").css({"color":"black"})
                $("#id_C979988").parent().parent().find('span.red').show()

            }   
           
        });




        $(document).on('change', '#id_C979989', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979990").val('')
            $("#id_C979991").val('')
            
            if(inputval==0){
                $("input[name='optional[C979990]']").prop( "checked", true );
                $("input[name='optional[C979991]']").prop( "checked", true );
               

                $("#id_C979990").removeAttr('required')            
                $("#id_C979990").attr('disabled', true)            
                $("#id_C979990").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979990").parent().parent().find('span.red').hide()
                //$("#id_C979990").val('')


                $("#id_C979991").removeAttr('required')            
                $("#id_C979991").attr('disabled', true)            
                $("#id_C979991").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979991").parent().parent().find('span.red').hide()
                //$("#id_C979991").val('')


            }else if(inputval==1){
                $("input[name='optional[C979990]']").prop( "checked", false );
                $("input[name='optional[C979991]']").prop( "checked", false );
                
                $("#id_C979990").attr('required',true)
                $("#id_C979990").attr('disabled', false)
                $("#id_C979990").css({"color":"black"})
                $("#id_C979990").parent().parent().find('span.red').show()

                $("#id_C979991").attr('required',true)
                $("#id_C979991").attr('disabled', false)
                $("#id_C979991").css({"color":"black"})
                $("#id_C979991").parent().parent().find('span.red').show()

            }   
           
        });

        $(document).on('change', '#id_C979992', function (event) {
            var inputval = parseInt($(this).val());

            $("#id_C979993").val('')
            $("#id_C979994").val('')
            
            if(inputval==0){
                $("input[name='optional[C979993]']").prop( "checked", true );
                $("input[name='optional[C979994]']").prop( "checked", true );
               

                $("#id_C979993").removeAttr('required')            
                $("#id_C979993").attr('disabled', true)            
                $("#id_C979993").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979993").parent().parent().find('span.red').hide()
                //$("#id_C979993").val('')


                $("#id_C979994").removeAttr('required')            
                $("#id_C979994").attr('disabled', true)            
                $("#id_C979994").css({"color":"rgb(235, 235, 228)"})
                $("#id_C979994").parent().parent().find('span.red').hide()
                //$("#id_C979994").val('')


            }else if(inputval==1){
                $("input[name='optional[C979993]']").prop( "checked", false );
                $("input[name='optional[C979994]']").prop( "checked", false );
                
                $("#id_C979993").attr('required',true)
                $("#id_C979993").attr('disabled', false)
                $("#id_C979993").css({"color":"black"})
                $("#id_C979993").parent().parent().find('span.red').show()

                $("#id_C979994").attr('required',true)
                $("#id_C979994").attr('disabled', false)
                $("#id_C979994").css({"color":"black"})
                $("#id_C979994").parent().parent().find('span.red').show()

            }   
           
        });

             
    }

/**
    *
    *NEUROPSYCHOLOGICAL ASSESSMENT BATTERY, JUGDEMENT (NABJ)
    */
    if(app_name=='C745190'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C437188' || fieldId=='id_C437189' 
                || fieldId=='id_C437190' || fieldId=='id_C437191' 
                || fieldId=='id_C437192' || fieldId=='id_C437193' 
                || fieldId=='id_C437194' || fieldId=='id_C437195' 
                || fieldId=='id_C437196' || fieldId=='id_C437197' 
                || fieldId=='id_C437198'){

                
                var id_C437188,id_C437189,id_C437190,id_C437191,id_C437192,id_C437193,
            id_C437194,id_C437195,id_C437196,id_C437197,
            total_raw;
                
                id_C437188 = $("#id_C437188").val();
                id_C437189 = $("#id_C437189").val();
                id_C437190 = $("#id_C437190").val();
                id_C437191 = $("#id_C437191").val();
                id_C437192 = $("#id_C437192").val();
                id_C437193 = $("#id_C437193").val();
                id_C437194 = $("#id_C437194").val();
                id_C437195 = $("#id_C437195").val();
                id_C437196 = $("#id_C437196").val();
                id_C437197 = $("#id_C437197").val();
                

                if(id_C437188==''){
                   id_C437188 = 0 
                }
                if(id_C437189==''){
                   id_C437189 = 0 
                }
                if(id_C437190==''){
                   id_C437190 = 0 
                }
                if(id_C437191==''){
                   id_C437191 = 0 
                }
                if(id_C437192==''){
                   id_C437192 = 0 
                }
                if(id_C437193==''){
                   id_C437193 = 0 
                }
                if(id_C437194==''){
                   id_C437194 = 0 
                }
                if(id_C437195==''){
                   id_C437195 = 0 
                }
                if(id_C437196==''){
                   id_C437196 = 0 
                }
                if(id_C437197==''){
                   id_C437197 = 0 
                }
                

                total_raw = parseInt(id_C437188)+parseInt(id_C437189)+
                parseInt(id_C437190)+parseInt(id_C437191)+parseInt(id_C437192)+
                parseInt(id_C437193)+parseInt(id_C437194)+parseInt(id_C437195)+
                parseInt(id_C437196)+parseInt(id_C437197);

                if(total_raw!=0){
                    $("#id_C437198").val(total_raw);
                }else{
                    $("#id_C437198").val('');
                }
              

            }
           
        });       
    }    


    /**
    *
    *FATIGUE SEVERITY SCALE (FSS)
    */
    if(app_name=='C113850'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C113944' || fieldId=='id_C113945' 
                || fieldId=='id_C113946' || fieldId=='id_C113947' 
                || fieldId=='id_C113948' || fieldId=='id_C113949' 
                || fieldId=='id_C113950' || fieldId=='id_C113951' 
                || fieldId=='id_C113952' || fieldId=='id_C113953'){

                
                var id_C113944,id_C113945,id_C113946,id_C113947,id_C113948,id_C113949,
            id_C113950,id_C113951,id_C113952,
            total_raw;
                
                id_C113944 = $("#id_C113944").val();
                id_C113945 = $("#id_C113945").val();
                id_C113946 = $("#id_C113946").val();
                id_C113947 = $("#id_C113947").val();
                id_C113948 = $("#id_C113948").val();
                id_C113949 = $("#id_C113949").val();
                id_C113950 = $("#id_C113950").val();
                id_C113951 = $("#id_C113951").val();
                id_C113952 = $("#id_C113952").val();
                

                if(id_C113944==''){
                   id_C113944 = 0 
                }
                if(id_C113945==''){
                   id_C113945 = 0 
                }
                if(id_C113946==''){
                   id_C113946 = 0 
                }
                if(id_C113947==''){
                   id_C113947 = 0 
                }
                if(id_C113948==''){
                   id_C113948 = 0 
                }
                if(id_C113949==''){
                   id_C113949 = 0 
                }
                if(id_C113950==''){
                   id_C113950 = 0 
                }
                if(id_C113951==''){
                   id_C113951 = 0 
                }
                if(id_C113952==''){
                   id_C113952 = 0 
                }
                
                
                
                total_raw = parseInt(id_C113944)+parseInt(id_C113945)+
                parseInt(id_C113946)+parseInt(id_C113947)+parseInt(id_C113948)+
                parseInt(id_C113949)+parseInt(id_C113950)+parseInt(id_C113951)+
                parseInt(id_C113952);

                
                if(total_raw!=0){
                    $("#id_C113953").val(total_raw);
                }else{
                    $("#id_C113953").val('');
                }
              

            }
           
        });       
    }


    /**
    *
    *REY AUDITORY VERBAL LEARNING TEST (RAVLT)
    */
    if(app_name=='C123655'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C123684' || fieldId=='id_C123686' 
                || fieldId=='id_C123687' || fieldId=='id_C123688' 
                || fieldId=='id_C123689' || fieldId=='id_C123690'){

                
                var id_C123684,id_C123686,id_C123687,id_C123688,id_C123689,id_C123689,id_C123690,total_raw;
                
                id_C123684 = $("#id_C123684").val();
                id_C123686 = $("#id_C123686").val();
                id_C123687 = $("#id_C123687").val();
                id_C123688 = $("#id_C123688").val();
                id_C123689 = $("#id_C123689").val();
                

                if(id_C123684==''){
                   id_C123684 = 0 
                }
                if(id_C123686==''){
                   id_C123686 = 0 
                }
                if(id_C123687==''){
                   id_C123687 = 0 
                }
                if(id_C123688==''){
                   id_C123688 = 0 
                }
                if(id_C123689==''){
                   id_C123689 = 0 
                }
                
                
                
                total_raw = parseInt(id_C123684)+parseInt(id_C123686)+
                parseInt(id_C123687)+parseInt(id_C123688)+parseInt(id_C123689);

                
                if(total_raw!=0){
                    $("#id_C123690").val(total_raw);
                }else{
                    $("#id_C123690").val('');
                }
              

            }
           
        });       
    }

    /**
    *
    *EPWORTH SLEEPINESS SCALE (ESS)
    */
    if(app_name=='C279074'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C281302' || fieldId=='id_C281303' 
                || fieldId=='id_C281304' || fieldId=='id_C281305' 
                || fieldId=='id_C281306' || fieldId=='id_C281307' 
                || fieldId=='id_C281308' || fieldId=='id_C281309' 
                || fieldId=='id_C281310'){

                
                var id_C281302,id_C281303,id_C281304,id_C281305,id_C281306,id_C281307
            ,id_C281308,id_C281309,id_C281310,total_raw;
                
                id_C281302 = $("#id_C281302").val();
                id_C281303 = $("#id_C281303").val();
                id_C281304 = $("#id_C281304").val();
                id_C281305 = $("#id_C281305").val();
                id_C281306 = $("#id_C281306").val();
                id_C281307 = $("#id_C281307").val();
                id_C281308 = $("#id_C281308").val();
                id_C281309 = $("#id_C281309").val();
                

                if(id_C281302==''){
                   id_C281302 = 0 
                }
                if(id_C281303==''){
                   id_C281303 = 0 
                }
                if(id_C281304==''){
                   id_C281304 = 0 
                }
                if(id_C281305==''){
                   id_C281305 = 0 
                }
                if(id_C281306==''){
                   id_C281306 = 0 
                }
                if(id_C281307==''){
                   id_C281307 = 0 
                }
                if(id_C281308==''){
                   id_C281308 = 0 
                }
                if(id_C281309==''){
                   id_C281309 = 0 
                }
                
                
                
                total_raw = parseInt(id_C281302)+parseInt(id_C281303)+
                parseInt(id_C281304)+parseInt(id_C281305)+parseInt(id_C281306)+
                parseInt(id_C281307)+parseInt(id_C281308)+parseInt(id_C281309);

                
                if(total_raw!=0){
                    $("#id_C281310").val(total_raw);
                }else{
                    $("#id_C281310").val('');
                }
              

            }
           
        });       
    }

    /**
    *
    *DIGIT VIGILANCE TEST (DVT)
    */
    if(app_name=='C365125'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C208201' || fieldId=='id_C208202' 
                || fieldId=='id_C208206' || fieldId=='id_C208207' 
                || fieldId=='id_C208209' || fieldId=='id_C208210' 
                || fieldId=='id_C208208' || fieldId=='id_C208211' 
                || fieldId=='id_C208203' || fieldId=='id_C208212'){

                
                var id_C208201,id_C208202,id_C208203,id_C208206,id_C208207,id_C208208,
            id_C208209,id_C208210,id_C208211,id_C208212,total_time_raw,pg1_total_err,
            pg2_total_err,total_err_raw_score;
                
                id_C208201 = $("#id_C208201").val();
                id_C208202 = $("#id_C208202").val();

                id_C208206 = $("#id_C208206").val();
                id_C208207 = $("#id_C208207").val();

                id_C208208 = $("#id_C208208").val();
                id_C208209 = $("#id_C208209").val();

                id_C208210 = $("#id_C208210").val();
                id_C208211 = $("#id_C208211").val();
                

                if(id_C208201==''){
                   id_C208201 = 0 
                }
                if(id_C208202==''){
                   id_C208202 = 0 
                }                
                
                if(id_C208206==''){
                   id_C208206 = 0 
                }
                if(id_C208207==''){
                   id_C208207 = 0 
                }
                if(id_C208208==''){
                   id_C208208 = 0 
                }
                if(id_C208209==''){
                   id_C208209 = 0 
                }
                if(id_C208210==''){
                   id_C208210 = 0 
                }
                if(id_C208211==''){
                   id_C208211 = 0 
                }
                
                
                
                total_time_raw = parseInt(id_C208201)+parseInt(id_C208202);
                pg1_total_err = parseInt(id_C208206)+parseInt(id_C208207);
                pg2_total_err = parseInt(id_C208209)+parseInt(id_C208210);
                total_err_raw_score = parseInt(id_C208208)+parseInt(id_C208211);

                
                if(total_time_raw!=0){
                    $("#id_C208203").val(total_time_raw);
                }else{
                    $("#id_C208203").val('');
                }

                if(pg1_total_err!=0){
                    $("#id_C208208").val(pg1_total_err);
                }else{
                    $("#id_C208208").val('');
                }

                if(pg2_total_err!=0){
                    $("#id_C208211").val(pg2_total_err);
                }else{
                    $("#id_C208211").val('');
                }

                if(total_err_raw_score!=0){
                    $("#id_C208212").val(total_err_raw_score);
                }else{
                    $("#id_C208212").val('');
                }
              

            }
           
        });       
    }

    /**
    *
    *WECHSLER MEMORY SCALE, 4TH EDITION, LOGICAL MEMORY, ADULT
    */
    if(app_name=='C466290'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C297826' || fieldId=='id_C297827' 
                || fieldId=='id_C297830' || fieldId=='id_C297831' 
                || fieldId=='id_C297828' || fieldId=='id_C297832'){

                
                var id_C297826,id_C297827,id_C297830,id_C297831,id_C297828,id_C297832,
            immd_rec_total_raw,delay_rec_total_raw;
                
                id_C297826 = $("#id_C297826").val();
                id_C297827 = $("#id_C297827").val();

                id_C297830 = $("#id_C297830").val();
                id_C297831 = $("#id_C297831").val();

                if(id_C297826==''){
                   id_C297826 = 0 
                }
                if(id_C297827==''){
                   id_C297827 = 0 
                }                
                
                if(id_C297830==''){
                   id_C297830 = 0 
                }
                if(id_C297831==''){
                   id_C297831 = 0 
                }
                
                
                
                immd_rec_total_raw = parseInt(id_C297826)+parseInt(id_C297827);
                delay_rec_total_raw = parseInt(id_C297830)+parseInt(id_C297831);

                
                if(immd_rec_total_raw!=0){
                    $("#id_C297828").val(immd_rec_total_raw);
                }else{
                    $("#id_C297828").val('');
                }

                if(delay_rec_total_raw!=0){
                    $("#id_C297832").val(delay_rec_total_raw);
                }else{
                    $("#id_C297832").val('');
                }
              

            }
           
        });       
    }

    /**
    *
    *EDINBURGH HANDEDNESS INVENTORY (EHI)
    */
    if(app_name=='C579048'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C435545' || fieldId=='id_C435546' 
                || fieldId=='id_C435547' || fieldId=='id_C435548' 
                || fieldId=='id_C435549' || fieldId=='id_C435550' 
                || fieldId=='id_C435551' || fieldId=='id_C435552' 
                || fieldId=='id_C435553'){

                
                var id_C435545,id_C435546,id_C435547,id_C435548,id_C435549,id_C435550,
            id_C435551,id_C435552,id_C435553,
            total_raw;
                
                id_C435545 = $("#id_C435545").val();
                id_C435546 = $("#id_C435546").val();
                id_C435547 = $("#id_C435547").val();
                id_C435548 = $("#id_C435548").val();
                id_C435549 = $("#id_C435549").val();
                id_C435550 = $("#id_C435550").val();
                id_C435551 = $("#id_C435551").val();
                id_C435552 = $("#id_C435552").val();
                
                

                if(id_C435545==''){
                   id_C435545 = 0 
                }
                if(id_C435546==''){
                   id_C435546 = 0 
                }
                if(id_C435547==''){
                   id_C435547 = 0 
                }
                if(id_C435548==''){
                   id_C435548 = 0 
                }
                if(id_C435549==''){
                   id_C435549 = 0 
                }
                if(id_C435550==''){
                   id_C435550 = 0 
                }
                if(id_C435551==''){
                   id_C435551 = 0 
                }
                if(id_C435552==''){
                   id_C435552 = 0 
                }
                
                
                
                total_raw = parseInt(id_C435545)+parseInt(id_C435546)+
                parseInt(id_C435547)+parseInt(id_C435548)+parseInt(id_C435549)+
                parseInt(id_C435550)+parseInt(id_C435551)+parseInt(id_C435552);

                
                if(total_raw!=0){
                    $("#id_C435553").val(total_raw);
                }else{
                    $("#id_C435553").val('');
                }
              

            }
           
        });       
    }

    /**
    *
    *DELIS-KAPLAN EXECUTIVE FUNCTIONING SYSTEM, LETTER FLUENCY (DKEFS-LF)
    */
    if(app_name=='C746333'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C282001' || fieldId=='id_C282002' 
                || fieldId=='id_C282003' || fieldId=='id_C282004'){

                
                var id_C282001,id_C282002,id_C282003,id_C282004,total_raw;
                
                id_C282001 = $("#id_C282001").val();
                id_C282002 = $("#id_C282002").val();
                id_C282003 = $("#id_C282003").val();
                
                

                if(id_C282001==''){
                   id_C282001 = 0 
                }
                if(id_C282002==''){
                   id_C282002 = 0 
                }
                if(id_C282003==''){
                   id_C282003 = 0 
                }
                
                
                
                
                total_raw = parseInt(id_C282001)+parseInt(id_C282002)+
                parseInt(id_C282003);

                
                if(total_raw!=0){
                    $("#id_C282004").val(total_raw);
                }else{
                    $("#id_C282004").val('');
                }
              

            }
           
        });       
    }


    /**
    *
    *DELIS-KAPLAN EXECUTIVE FUNCTIONING SYSTEM, CATEGORY FLUENCY (DKEFS-CF)
    */
    if(app_name=='C779861'){
        
        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');

            if(fieldId=='id_C650499' || fieldId=='id_C650500' 
                || fieldId=='id_C650501'){

                
                var id_C650499,id_C650500,id_C650501,total_raw;
                
                id_C650499 = $("#id_C650499").val();
                id_C650500 = $("#id_C650500").val();
                
                

                if(id_C650499==''){
                   id_C650499 = 0 
                }
                if(id_C650500==''){
                   id_C650500 = 0 
                }
                
                
                
                total_raw = parseInt(id_C650499)+parseInt(id_C650500);

                
                if(total_raw!=0){
                    $("#id_C650501").val(total_raw);
                }else{
                    $("#id_C650501").val('');
                }
              

            }
           
        });       
    }

    /**
    *
    *DELIS-KAPLAN EXECUTIVE FUNCTIONING SYSTEM, CATEGORY FLUENCY (DKEFS-CF)
    */
    if(app_name=='C818261'){
        
        $(document).on('keyup change', 'select', function (event) {
            var fieldId = $(this).attr('id');
            

            if(fieldId=='id_C283780' || fieldId=='id_C283781' 
                || fieldId=='id_C283782' || fieldId=='id_C283783' 
                || fieldId=='id_C283784' || fieldId=='id_C283785' 
                || fieldId=='id_C283786' || fieldId=='id_C283787' 
                || fieldId=='id_C283790' || fieldId=='id_C283791' 
                || fieldId=='id_C283792' || fieldId=='id_C283796' 
                || fieldId=='id_C283797' || fieldId=='id_C283801' 
                || fieldId=='id_C283802' || fieldId=='id_C283803' 
                || fieldId=='id_C283804' || fieldId=='id_C283805' 
                || fieldId=='id_C283806' || fieldId=='id_C283808' 
                || fieldId=='id_C283807' || fieldId=='id_C283788' 
                || fieldId=='id_C283789' || fieldId=='id_C283793' 
                || fieldId=='id_C283794' || fieldId=='id_C283795' 
                || fieldId=='id_C283798' || fieldId=='id_C283799' 
                || fieldId=='id_C283800'){

                
                var id_C283780,id_C283781,id_C283782,id_C283783,id_C283784,id_C283785,
            id_C283786,id_C283787,id_C283790,id_C283791,id_C283792,id_C283796,id_C283797,
            id_C283801,id_C283802,id_C283803,id_C283804,id_C283805,id_C283806,id_C283808,
            id_C283788,id_C283789,id_C283793,id_C283794,id_C283795,id_C283798,
            id_C283799,id_C283800,total_raw;
                
                id_C283780 = $("#id_C283780").val();
                id_C283781 = $("#id_C283781").val();
                id_C283782 = $("#id_C283782").val();
                id_C283783 = $("#id_C283783").val();
                id_C283784 = $("#id_C283784").val();
                id_C283785 = $("#id_C283785").val();
                id_C283786 = $("#id_C283786").val();
                id_C283787 = $("#id_C283787").val();
                id_C283790 = $("#id_C283790").val();
                id_C283791 = $("#id_C283791").val();
                id_C283792 = $("#id_C283792").val();
                id_C283796 = $("#id_C283796").val();
                id_C283797 = $("#id_C283797").val();
                id_C283801 = $("#id_C283801").val();
                id_C283802 = $("#id_C283802").val();
                id_C283803 = $("#id_C283803").val();
                id_C283804 = $("#id_C283804").val();
                id_C283805 = $("#id_C283805").val();
                id_C283806 = $("#id_C283806").val();
                id_C283808 = $("#id_C283808").val();

                id_C283788 = $("#id_C283788").val();
                id_C283789 = $("#id_C283789").val();
                id_C283793 = $("#id_C283793").val();
                id_C283794 = $("#id_C283794").val();
                id_C283795 = $("#id_C283795").val();
                id_C283798 = $("#id_C283798").val();
                id_C283799 = $("#id_C283799").val();
                id_C283800 = $("#id_C283800").val();
                
                

                if(id_C283780=='' || id_C283780==0){
                   id_C283780 = 0 
                }
                if(id_C283781=='' || id_C283781==0){
                   id_C283781 = 0 
                }
                if(id_C283782=='' || id_C283782==0){
                   id_C283782 = 0 
                }
                if(id_C283783=='' || id_C283783==0){
                   id_C283783 = 0 
                }
                if(id_C283784=='' || id_C283784==0){
                   id_C283784 = 0 
                }
                if(id_C283785=='' || id_C283785==0){
                   id_C283785 = 0 
                }
                if(id_C283786=='' || id_C283786==0){
                   id_C283786 = 0 
                }
                if(id_C283787=='' || id_C283787==0){
                   id_C283787 = 0 
                }
                if(id_C283790=='' || id_C283790==0){
                   id_C283790 = 0 
                }
                if(id_C283791=='' || id_C283791==0){
                   id_C283791 = 0 
                }
                if(id_C283792=='' || id_C283792==0){
                   id_C283792 = 0 
                }
                if(id_C283796=='' || id_C283796==0){
                   id_C283796 = 0 
                }
                if(id_C283797=='' || id_C283797==0){
                   id_C283797 = 0 
                }
                if(id_C283801=='' || id_C283801==0){
                   id_C283801 = 0 
                }
                if(id_C283802=='' || id_C283802==0){
                   id_C283802 = 0 
                }
                if(id_C283803=='' || id_C283803==0){
                   id_C283803 = 0 
                }
                if(id_C283804=='' || id_C283804==0){
                   id_C283804 = 0 
                }
                if(id_C283805=='' || id_C283805==0){
                   id_C283805 = 0 
                }
                if(id_C283806=='' || id_C283806==0){
                   id_C283806 = 0 
                }
                if(id_C283808=='' || id_C283808==0){
                   id_C283808 = 0 
                }

                if(id_C283788==''){
                   id_C283788 = 0 
                }
                if(id_C283789==''){
                   id_C283789 = 0 
                }
                if(id_C283793==''){
                   id_C283793 = 0 
                }
                if(id_C283794==''){
                   id_C283794 = 0 
                }
                if(id_C283795==''){
                   id_C283795 = 0 
                }
                if(id_C283798==''){
                   id_C283798 = 0 
                }
                if(id_C283799==''){
                   id_C283799 = 0 
                }
                if(id_C283800==''){
                   id_C283800 = 0 
                }                
                
                
                total_raw = parseInt(id_C283780)+parseInt(id_C283781)+parseInt(id_C283782)+
                parseInt(id_C283783)+parseInt(id_C283784)+parseInt(id_C283785)+
                parseInt(id_C283786)+parseInt(id_C283787)+parseInt(id_C283790)+
                parseInt(id_C283791)+parseInt(id_C283792)+parseInt(id_C283796)+
                parseInt(id_C283797)+parseInt(id_C283801)+parseInt(id_C283802)+
                parseInt(id_C283803)+parseInt(id_C283804)+parseInt(id_C283805)+
                parseInt(id_C283806)+parseInt(id_C283808)+parseInt(id_C283788)+
                parseInt(id_C283789)+parseInt(id_C283793)+parseInt(id_C283794)+
                parseInt(id_C283795)+parseInt(id_C283798)+parseInt(id_C283799)+
                parseInt(id_C283800);

                
                if(total_raw!=0){
                    $("#id_C283807").val(total_raw);
                }else{
                    $("#id_C283807").val('');
                }
              

            }
           
        });


        $(document).on('keyup change', 'input', function (event) {
            var fieldId = $(this).attr('id');
            

            if(fieldId=='id_C283780' || fieldId=='id_C283781' 
                || fieldId=='id_C283782' || fieldId=='id_C283783' 
                || fieldId=='id_C283784' || fieldId=='id_C283785' 
                || fieldId=='id_C283786' || fieldId=='id_C283787' 
                || fieldId=='id_C283790' || fieldId=='id_C283791' 
                || fieldId=='id_C283792' || fieldId=='id_C283796' 
                || fieldId=='id_C283797' || fieldId=='id_C283801' 
                || fieldId=='id_C283802' || fieldId=='id_C283803' 
                || fieldId=='id_C283804' || fieldId=='id_C283805' 
                || fieldId=='id_C283806' || fieldId=='id_C283808' 
                || fieldId=='id_C283807' || fieldId=='id_C283788' 
                || fieldId=='id_C283789' || fieldId=='id_C283793' 
                || fieldId=='id_C283794' || fieldId=='id_C283795' 
                || fieldId=='id_C283798' || fieldId=='id_C283799' 
                || fieldId=='id_C283800'){

                
                var id_C283780,id_C283781,id_C283782,id_C283783,id_C283784,id_C283785,
            id_C283786,id_C283787,id_C283790,id_C283791,id_C283792,id_C283796,id_C283797,
            id_C283801,id_C283802,id_C283803,id_C283804,id_C283805,id_C283806,id_C283808,
            id_C283788,id_C283789,id_C283793,id_C283794,id_C283795,id_C283798,
            id_C283799,id_C283800,total_raw;
                
                id_C283780 = $("#id_C283780").val();
                id_C283781 = $("#id_C283781").val();
                id_C283782 = $("#id_C283782").val();
                id_C283783 = $("#id_C283783").val();
                id_C283784 = $("#id_C283784").val();
                id_C283785 = $("#id_C283785").val();
                id_C283786 = $("#id_C283786").val();
                id_C283787 = $("#id_C283787").val();
                id_C283790 = $("#id_C283790").val();
                id_C283791 = $("#id_C283791").val();
                id_C283792 = $("#id_C283792").val();
                id_C283796 = $("#id_C283796").val();
                id_C283797 = $("#id_C283797").val();
                id_C283801 = $("#id_C283801").val();
                id_C283802 = $("#id_C283802").val();
                id_C283803 = $("#id_C283803").val();
                id_C283804 = $("#id_C283804").val();
                id_C283805 = $("#id_C283805").val();
                id_C283806 = $("#id_C283806").val();
                id_C283808 = $("#id_C283808").val();

                id_C283788 = $("#id_C283788").val();
                id_C283789 = $("#id_C283789").val();
                id_C283793 = $("#id_C283793").val();
                id_C283794 = $("#id_C283794").val();
                id_C283795 = $("#id_C283795").val();
                id_C283798 = $("#id_C283798").val();
                id_C283799 = $("#id_C283799").val();
                id_C283800 = $("#id_C283800").val();
                
                

                if(id_C283780=='' || id_C283780==0){
                   id_C283780 = 0 
                }
                if(id_C283781=='' || id_C283781==0){
                   id_C283781 = 0 
                }
                if(id_C283782=='' || id_C283782==0){
                   id_C283782 = 0 
                }
                if(id_C283783=='' || id_C283783==0){
                   id_C283783 = 0 
                }
                if(id_C283784=='' || id_C283784==0){
                   id_C283784 = 0 
                }
                if(id_C283785=='' || id_C283785==0){
                   id_C283785 = 0 
                }
                if(id_C283786=='' || id_C283786==0){
                   id_C283786 = 0 
                }
                if(id_C283787=='' || id_C283787==0){
                   id_C283787 = 0 
                }
                if(id_C283790=='' || id_C283790==0){
                   id_C283790 = 0 
                }
                if(id_C283791=='' || id_C283791==0){
                   id_C283791 = 0 
                }
                if(id_C283792=='' || id_C283792==0){
                   id_C283792 = 0 
                }
                if(id_C283796=='' || id_C283796==0){
                   id_C283796 = 0 
                }
                if(id_C283797=='' || id_C283797==0){
                   id_C283797 = 0 
                }
                if(id_C283801=='' || id_C283801==0){
                   id_C283801 = 0 
                }
                if(id_C283802=='' || id_C283802==0){
                   id_C283802 = 0 
                }
                if(id_C283803=='' || id_C283803==0){
                   id_C283803 = 0 
                }
                if(id_C283804=='' || id_C283804==0){
                   id_C283804 = 0 
                }
                if(id_C283805=='' || id_C283805==0){
                   id_C283805 = 0 
                }
                if(id_C283806=='' || id_C283806==0){
                   id_C283806 = 0 
                }
                if(id_C283808=='' || id_C283808==0){
                   id_C283808 = 0 
                }

                if(id_C283788==''){
                   id_C283788 = 0 
                }
                if(id_C283789==''){
                   id_C283789 = 0 
                }
                if(id_C283793==''){
                   id_C283793 = 0 
                }
                if(id_C283794==''){
                   id_C283794 = 0 
                }
                if(id_C283795==''){
                   id_C283795 = 0 
                }
                if(id_C283798==''){
                   id_C283798 = 0 
                }
                if(id_C283799==''){
                   id_C283799 = 0 
                }
                if(id_C283800==''){
                   id_C283800 = 0 
                }                
                
                
                total_raw = parseInt(id_C283780)+parseInt(id_C283781)+parseInt(id_C283782)+
                parseInt(id_C283783)+parseInt(id_C283784)+parseInt(id_C283785)+
                parseInt(id_C283786)+parseInt(id_C283787)+parseInt(id_C283790)+
                parseInt(id_C283791)+parseInt(id_C283792)+parseInt(id_C283796)+
                parseInt(id_C283797)+parseInt(id_C283801)+parseInt(id_C283802)+
                parseInt(id_C283803)+parseInt(id_C283804)+parseInt(id_C283805)+
                parseInt(id_C283806)+parseInt(id_C283808)+parseInt(id_C283788)+
                parseInt(id_C283789)+parseInt(id_C283793)+parseInt(id_C283794)+
                parseInt(id_C283795)+parseInt(id_C283798)+parseInt(id_C283799)+
                parseInt(id_C283800);

                
                if(total_raw!=0){
                    $("#id_C283807").val(total_raw);
                }else{
                    $("#id_C283807").val('');
                }
              

            }
           
        });

        
    }