/**
*
*Get and validate from healthy brains
*/

$('#connect_btn').click(function() {

    $("#connect_with_healthy_brain").validate({
    rules: {
        // simple rule, converted to {required:true}
        passcode: "required",
        // compound rule
        patient_email: {
          required: true,
          email: true
        }
  },
  submitHandler: function(form) {
    // do other things for a valid form
    //form.submit();
    api_call()
  }
});

    
});

/**
*Call api
*/
function api_call(){

    var csrftoken = getCookie('csrftoken');
    $.ajax({
        headers: {
            'X-CSRFToken': csrftoken
        },
        type: 'POST',
        url: connectwithhealthybrain,
        data: $("#connect_with_healthy_brain").serialize(),
        success: function(response) {
           $("#connect_with_healthy_brain").removeAttr('data-dirty');
           location.reload();
           
        },
        error: function(jqXHR, textStatus, errorThrown) {
                if ($.isFunction(self.options.onError)) {
                    self.options.onError(jqXHR, textStatus, errorThrown);
                } else {
                    ajaxCallback(false);
                }
            },
    });
}