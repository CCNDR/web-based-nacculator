function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


// function ckEditor(textarea = null, url = null, uploadImagePath = null, browseImageUrl = null) {

//     CKEDITOR.extraPlugins = 'tokens';
//     var csrftoken = getCookie('csrftoken');
//     CKEDITOR.replace(textarea, {
//         filebrowserBrowseUrl: browseImageUrl,
//         filebrowserUploadUrl: url,
//     });
//     CKEDITOR.extraPlugins = 'uploadimage'

// }

function ckEditor() {
  var textarea = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var url = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var uploadImagePath = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var browseImageUrl = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  CKEDITOR.extraPlugins = 'tokens';
  var csrftoken = getCookie('csrftoken');
  CKEDITOR.replace(textarea, {
    filebrowserBrowseUrl: browseImageUrl,
    filebrowserUploadUrl: url
  });
  CKEDITOR.extraPlugins = 'uploadimage';
}


function dropdownMenu() {

    var dropdown = $('span#cke_85').html('<span><div class="dropdown">' +
        '<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButtonbtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
        'Tag Variables</button>' +
        '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">' +
        '<a class="dropdown-item tag-variable" value="##FIRSTNAME##">First Name</a>' +
        '<a class="dropdown-item tag-variable" value="##LASTNAME##">Last Name</a>' +
        '<a class="dropdown-item tag-variable" value="##DOB##">DOB</a>' +
        '<a class="dropdown-item tag-variable" value="##AGE##">Age</a>' +
        '<a class="dropdown-item tag-variable" value="##HAND##">Hand</a>' +
        '<a class="dropdown-item tag-variable" value="##EXAMINER##">Examiner</a>' +
        '<a class="dropdown-item tag-variable" value="##TESTDATE##">Testdate</a>' +
        '<a class="dropdown-item tag-variable" value="##MRN##">MRN</a>' +
        '<a class="dropdown-item tag-variable" value="##GENDER##">Gender</a>' +
        '<a class="dropdown-item tag-variable" value="##EDUCATION##">Education</a>' +
        '</div></div></span>')

    return dropdown
}

$('.show-template-checkbox').click(function() {
    $(".score_container").addClass('hidden').hide();
    $("#edit_score_type").prop('checked',false);


    var textarea = $(this).parents('form').find('textarea');
    $('#cke_edit-template').css({
        "margin-top": "-27%",
        "display": "block"
    })

    if ($(this).is(':checked')) {
        textarea.html('')
        var dataId = $(this).attr('data-id');
        var csrftoken = getCookie('csrftoken');
        $.ajax({
            headers: {
                'X-CSRFToken': csrftoken
            },
            type: 'POST',
            url: getTemplateUrl,
            data: {
                dataId: dataId,
                templateName: 'defaultreportgrid.html'
            },
            success: function(data) {
                textarea.text(data.template)
                textarea.removeAttr('hidden').show()
                ckEditor(textarea.attr('name'), uploadImageUrl, uploadImagePath, browseImageUrl)
                $('#cke_edit-template').show()
                setTimeout(function() {
                    dropdownMenu();
                }, 200);
                setTimeout(function() {
                    tagVariables();
                }, 200);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                if ($.isFunction(self.options.onError)) {
                    self.options.onError(jqXHR, textStatus, errorThrown);
                } else {
                    ajaxCallback(false);
                }
            },
        });

    } else {
        textarea.addClass('hidden').hide()
        $('#cke_edit-template').hide()
    }
});


function tagVariables() {
    $('.tag-variable').click(function() {
        var myValue = $(this).attr('value')
        console.log($('textarea#edit-template'))
        var cursorPosition = $('textarea#edit-template').prop("selectionStart");
        var myField = $('textarea#edit-template')[0];

        if (document.selection) {
            myField.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
        } else if (myField.selectionStart || myField.selectionStart == '0') {

            var startPos = myField.selectionStart;
            var endPos = myField.selectionEnd;

            myField.value = myField.value.substring(0, cursorPosition) + myValue + myField.value.substring(endPos, myField.value.length);
            CKEDITOR.instances['edit-template'].insertText(myValue);
        } else {
            myField.value += myValue;
        }

    })

}

/**
*
*Manage checkbox toogle
*/

$('#edit_score_type').click(function() {
    $('.show-template-checkbox').prop('checked', false);
    $(".edit-template").addClass('hidden').hide();
    $('#cke_edit-template').hide();

    if ($(this).is(':checked')) {
        $(".score_container").removeAttr('hidden').show();
        $(".score_container").removeClass('hidden').show();


        var dataId = $(this).attr('data-id');
        var csrftoken = getCookie('csrftoken');

        $.ajax({
            headers: {
                'X-CSRFToken': csrftoken
            },
            type: 'POST',
            url: getUserScoreUrl,
            data: {
                dataId: dataId,
            },
            success: function(response) {

                $.each(response.scoreData, function(key,value) {

                    //$("#"+value.test_short_name+"_converted_score_type").val(value.changed_score_type);
                    $("#percentile_rank_name").val(value.percentile_rank_name);

                });

            },
            error: function(jqXHR, textStatus, errorThrown) {
                if ($.isFunction(self.options.onError)) {
                    self.options.onError(jqXHR, textStatus, errorThrown);
                } else {
                    ajaxCallback(false);
                }
            },
        });
    }else{
        $(".score_container").addClass('hidden').hide();
        $(this).prop('checked',false);
    }

});