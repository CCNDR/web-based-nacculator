# These commands will stop any Docker container running then remove them.  
# Please be aware this will remove all docker insances running or stalled.
# This will not completely remove all images you will need to run the following command to complete this:
# docker rmi $(docker images -q)

docker kill $(docker ps -q)
docker rm $(docker ps -a -q)

read -p "Do you want to remove Docker images completely? (Warning: this will force the download of all docker images on rebuild) (y/n)" answer

case $answer in
  y)
    docker rmi -f $(docker images -q)
    ;;
  n)
    ;;
  *)
    ;;
esac