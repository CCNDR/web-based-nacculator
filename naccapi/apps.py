from django.apps import AppConfig


class NaccapiConfig(AppConfig):
    name = 'naccapi'
