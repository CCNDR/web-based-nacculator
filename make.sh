#! /bin/bash
# set env
source $(dirname "$0")/env.sh
# Make Migrations
docker-compose run web python3 manage.py makemigrations --settings=$DJANGO_SETTINGS_MODULE

# Complete migration
docker-compose run web python3 manage.py migrate --settings=$DJANGO_SETTINGS_MODULE
