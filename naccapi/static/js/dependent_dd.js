
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/**
*
*Get value of dependent dropdown
*/


$('form#testnamesummarysheet_form select#id_cat_id').change(function() {
    var urlval = $(location).attr('href'),
    partsval = urlval.split("/"),
    app_name_val = partsval[partsval.length-1];

    if(urlval.indexOf('/admin/master/testnamesummarysheet') != -1){

        app_name_val = partsval[partsval.length-2];
    }

    var catid = $(this).val();

    getsubcategory(catid, '', app_name_val);
});

$(document).ready(function() {
    //$("#id_sub_cat_id").html("<option value=''>---------</option>");

    var urlval = $(location).attr('href'),
    partsval = urlval.split("/"),
    app_name_val = partsval[partsval.length-1];

    if(urlval.indexOf('/admin/master/testnamesummarysheet') != -1){

        app_name_val = partsval[partsval.length-2];
    }


    var updated_id = ''
    if (app_name_val=='change'){

        updated_id = partsval[partsval.length-3];

        var catid = $("#id_cat_id").val();
        var sub_cat_id = $("#id_sub_cat_id").val(); 
        getsubcategory(catid, sub_cat_id, app_name_val);

    }
    if (app_name_val=='add'){
        $("#id_sub_cat_id").html('<option value="">---------</option>');

    }

});


function getsubcategory(catid, sub_cat_id='', mode=''){

    var csrftoken = getCookie('csrftoken');

    $.ajax({
    headers: {
        'X-CSRFToken': csrftoken
    },
    type: 'POST',
    url: getSubCatUrl,
    data: {
        dataId: catid,
    },
    success: function(response) {

        if(response.subcatedata.length > 0 && sub_cat_id!='' && mode=='change'){
            var optiontxt = "";
        }
        /*else if(response.subcatedata.length > 0 && mode=='add'){
            var optiontxt = "";
        }*/
        else{            
            var optiontxt = "<option value=''>---------</option>";
        }
        

        $.each(response.subcatedata, function(key,value) {
            optiontxt = optiontxt + "<option value='"+value.id+"'>"+value.val+"</option>";
            
            });

        $("#id_sub_cat_id").html(optiontxt);

        if(sub_cat_id!=''){

            $("#id_sub_cat_id").val(sub_cat_id)

        }

        

    }
    
    });
}