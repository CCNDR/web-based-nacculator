let profileApp = (function(){

    let version = '1.0';

    function init(){
        console.log('init');
        $uploadCrop = $('#upload-crop-img').croppie({
            // viewport: { width: 640, height: 355 },
            // boundary: { width: 700, height: 410 },
            viewport: { width: 200, height: 200 },
            boundary: { width: 700, height: 410 },

        });
        $('#crop-image-modal').on('shown.bs.modal', function(){
            $uploadCrop.croppie('bind', {
                url: rawImg,
                point: [0,0,0,0]
            }).then(function(){

            });
        });
        $('#crop-image-btn').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type:'base64',
                format: 'jpeg',
                size: 'original'
            }).then(function (resp) {
                $('#profile-preview').attr('src', resp);
                $('input[type=hidden][name=cropped_image]').val(resp);
                $('#crop-image-modal').modal('hide');
            });

        });

        $("#avatar_clear_id").on('change', function(e){
            if($(this).is(":checked")){
                $(this).parent().addClass('clear');
            } else {
                $(this).parent().removeClass('clear');
            }
        });
    };
    let initCroppie = function initCroppie(elm){
        if($(elm).length){
            $(document).on('change', elm ,function () {
                readFile(this);
            });
        }
    };
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate image dimension
                image.onload = function () {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#crop-image-modal').modal('show');
                        rawImg = e.target.result;
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        }
        else {
            alert("Sorry - your browser doesn't support the FileReader API");
        }
    }
    return {
        init: function(){
            init();
        },
        initCroppie: function(elm){
            initCroppie(elm);
        },
    };
}());


$(document).ready(function(){
    profileApp.init();
    profileApp.initCroppie("input[type=file][name=profile_avatar]");

    $("#id_mflocation").append("<option value='create_new'>Create New</option>");

    $("#id_mflocation").change(function(){
        if($(this).val()=='create_new'){
            window.open('/cleveland/patcreate','_blank');
        }
    })
});

