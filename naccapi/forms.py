import datetime

from django import forms


class AssessFileForm(forms.Form):
    # importFile = forms.FileField()
    importFile = forms.FileField(required=True, label="File")
    filterby = forms.CharField(required=True, label="Filter")
    # importFile = forms.FileField(widget=forms.FileInput)

