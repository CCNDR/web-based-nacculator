let assessExport = (function(){

    let version = '1.0';

    function init(){
        console.log('init');
    };
    let applySelect2 = function applySelect2(elm){
        if($(elm).length){
            // console.log('applySelect2');
            // console.log(typeof select2);
            $(".upload-cols").select2();
        }
    };
    function bindBrowse(){
        $('.importFile').on('change', function(e){
            if(e.target.files.length){
                console.log(e.target.files[0].name);
                $(this).closest('form.importForm').find('.inputFileName').text(e.target.files[0].name);
                $(this).closest('form.importForm').find('.inputFileName').attr('title', e.target.files[0].name);
            }
            return false;
        });
        $('.filterFile').on('change', function(e){
            if(e.target.files.length){
                console.log(e.target.files[0].name);
                $(this).closest('form.importForm').find('.filterFileName').text(e.target.files[0].name);
                $(this).closest('form.importForm').find('.filterFileName').attr('title', e.target.files[0].name);
            }
            return false;
        });
    };
    let bindExaminer = function bindExaminer(){
        $(".assess_exam_id:first").on('change', function(e){
            $(".assess_exam_id").not(this).val($(this).val());
        });
    };
    let bindClinician = function bindClinician(){
        $(".assess_clin_id:first").on('change', function(e){
            $(".assess_clin_id").not(this).val($(this).val());
        });
    };
    return {
        init: function(){
            init();
            bindBrowse();
            bindExaminer();
            bindClinician();
        },
        applySelect2: function(elm){
            applySelect2(elm);
        },
    };
}());


$(document).ready(function(){
    assessExport.init();
    assessExport.applySelect2(".upload-cols");
});

function filterPopupHelp() {
  var popup = document.getElementById("filterPopup");
  popup.classList.toggle("show");
}
function didPopupHelp() {
  var popup = document.getElementById("didPopup");
  popup.classList.toggle("show");
}

$('#popup1').popup();


$('#popup2').popup({
  pagecontainer: '#page',
  escape: false
});
