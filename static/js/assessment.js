let test_filter = [];

// let intersection = function (a, b) {
//     a = new Set(a), b = new Set(b);
//     return [...a].filter(v => b.has(v));
// };

let intersection = function (a, b) {
    return a.filter(function(n) {
        return b.indexOf(n) !== -1;
    });
};

// includes polyfill
if (!Array.prototype.includes) {
    Array.prototype.includes = function (elt) { return this.indexOf(elt) !== -1; }
}
if (!String.prototype.includes) {
  String.prototype.includes = function(search, start) {
    'use strict';

    if (search instanceof RegExp) {
      throw TypeError('first argument must not be a RegExp');
    }
    if (start === undefined) { start = 0; }
    return this.indexOf(search, start) !== -1;
  };
}

let apply_filter = function () {
    $('div.test-btn').each(function (i, t) {
        let test_div = $(t);
        let test_categories = test_div.data('categoryArray');
        if (intersection(test_categories, test_filter).length == 0) {
            test_div.hide();
        } else {
            test_div.show();
        }
    });
};

let clear_filter = function () {
    // console.log("clear_filter()");
    $('[data-category-all]').show();
    $("#filt_all").prop('checked', true);
    $("#filt_all").parent().addClass('active');
    test_filter = [];
};

let set_filter = function (state, category) {
    if (state) {
        if (!test_filter.includes(category)) {
            test_filter.push(category);
        }
    } else {
        if (test_filter.includes(category)) {
            // test_filter = test_filter.filter(x => x != category);
            test_filter = test_filter.filter(function (x) {
              return x != category;
            });
        }
    }
    if (test_filter.length == 0) {
        clear_filter();
        $("#filt_all").parent().addClass('active');
    } else {
        apply_filter();
        $("#filt_all").parent().removeClass('active');
    }
}

let add_test = function(testElmRef){
    //selected test
    var testId = testElmRef.find('input[name="var_id[]"]').val();
    var testName = testElmRef.find('span.test-panel-link').text();
    testElmRef.find('label.test-panel').addClass('active');
    testElmRef.find('label.test-panel input').prop('checked', true);
    // update ordering
    update_ordering(testId, testName, true);

    return false;
}

let remove_test = function(testElmRef){
    //unselect test
    var testId = testElmRef.find('input[name="var_id[]"]').val();
    var testName = testElmRef.find('span.test-panel-link').text();
    testElmRef.find('label.test-panel').removeClass('active');
    testElmRef.find('label.test-panel input').prop('checked', false);
    // update ordering
    update_ordering(testId, testName, false);

    return false;
}

let select_all_test = function(category, isChecked){
    selectedCategories = [];
    $(".selectAll:checked").each(function(){
        selectedCategories.push($(this).val());
    });

    var catItems = $("div.testListBlock div.test-btn").each(function(){
        itemCatList = $(this).data('categoryArray');
        //check if test belong to selected category
        if(category == '_all_' || ($.inArray(category, itemCatList) !== -1)){
            var testCode = $(this).find('input[name="var_id[]"]').val();
            if (isChecked && !$(this).find('.btn.test-panel').hasClass('active')){
                //select only unselected tests in category
                // $(this).find('label.test-panel').click();
                add_test($(this))
            } else if(!isChecked && $(this).find('.btn.test-panel').hasClass('active')){
                //unselect only selected tests in cateogry
                // remove_test($(this));
                //remove only if not in other selected category
                if(intersection(selectedCategories, itemCatList).length == 0){
                    remove_test($(this));
                }
            }
        }
    });
    return false;
}

let update_ordering = function(testId, testName, isChecked){
    if(isChecked === true){
        $(".selectedTest div.noDataMsg").hide();
        if (!$(".selectedTest #sortableTestBlock").find("li#"+testId).length){
            $(".selectedTest #sortableTestBlock").append('<li id="'+testId+'" data-id="'+testId+'"><label class="testTag">'
                + '<span>'+testName+'</span>'
                + '<i class="fas fa-times discardTest" data-test-id="'+testId+'"></i>'
            + '</label></li>');
        }
        if($(".selectedTest li").length){
            $(".selectedTest div.noDataMsg").hide();
        }
        // if all test selected from a category,check select all checkbox of that category
        //
    }else if (isChecked == false){
        if ($(".selectedTest #sortableTestBlock").find("li#"+testId).length){
            $(".selectedTest #sortableTestBlock").find('li#'+testId).remove();
        }
        if(!$(".selectedTest li").length){
            $(".selectedTest div.noDataMsg").show();
        }
        // if at least one test unselected from a category,uncheck select all checkbox of that category
        //
    }

    // update sort order
    update_sort_order();
}

let update_sort_order = function(){
    //update sort order
    var ordering = $('#sortableTestBlock').sortable('toArray',{ attribute: "data-id" });
    ordering = JSON.stringify(ordering)
    $(".selectedTest input[type=hidden][name=ordering]").val(ordering);
}

function confirmCallback(testId) {
    console.log('ID: ' + testId);
}

$(document).ready(function () {
    console.log('Assessment Creation Page')

    $('input[name="filter"]').change(function (e) {

        var btn = $(this);
        // console.log('Toggle filter for category: ' + btn.val(), ' state: ' + btn.prop('checked'));
        set_filter(btn.prop('checked'), btn.val());
    });


    $(".testSelectAll").on('change.selectAll', "input[name=selectAll]", function(e){
        // if filter not active, activate it
        if(!$(this).closest('.testTypeLoop').find('label.activeOnOff').hasClass('active')){
            $(this).closest('.testTypeLoop').find('.activeOnOff').click();
        }

        var isChecked = $(this).is(":checked") ? true:false;
        var selectedCategory = $(this).val();

        // apply select all
        select_all_test(selectedCategory, isChecked);

        return false;
    });


    $("#filt_all").change(function () {
        clear_filter();
        $('input[name="filter"]').prop('checked', false);
        $('label.btn.btn-sort').removeClass('active');
    });
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        btnOkClass: 'btn btn-success',
        btnOkIconClass: 'glyphicon',
        btnCancelClass: 'btn btn-danger',
        btnCancelIconClass: 'glyphicon'
    })
    .on('confirmed.bs.confirmation', function (e) {
        var testId = '#' + e.delegateTarget.dataset.testId;
        $(testId + ' label').removeClass('active');
        $(testId + ' label input').prop('checked', '');
        $(e.target).off();

        // ###ORDERING###: update ordering markup
        $(".selectedTest").find('li'+testId).remove();
        if(!$(".selectedTest ul#sortableTestBlock li").length){
            $(".selectedTest div.noDataMsg").show();
        }
        update_sort_order();
        return false;
    });

    // ###ORDERING###: Add/remove ordering markup
    $(document).on("change", "div.test-btn", function(e){
        var testId = $(this).find('input[type="checkbox"][name="var_id[]"]').val();
        var testName = $(this).find('span.test-panel-link').text();
        var isChecked = $(this).find('input[type="checkbox"][name="var_id[]"]').is(":checked");

        update_ordering(testId, testName, isChecked);

        return false;
    });

    // ###ORDERING###: remove newly saved without confirmation
    $("body").on('click', ".discardTest", function(e){
        var testId = '#' + e.target.dataset.testId;
        var testId = '#' + e.target.dataset.testId;
        $(testId + ' label').removeClass('active');
        $(testId + ' label input').prop('checked', '');
        $(e.target).off();

        // ###ORDERING###: update ordering markup
        $(".selectedTest ul#sortableTestBlock").find('li'+testId).remove();
        if(!$(".selectedTest ul#sortableTestBlock li").length){
            $(".selectedTest div.noDataMsg").show();
        }
        update_sort_order();
    });
    // ###ORDERING###: remove previously saved with confirmation
    $('body').confirmation({
        selector: "#sortableTestBlock .testTag .confirmDiscard",
        // rootSelector: '[data-toggle=confirmation]',
        btnOkClass: 'btn btn-success',
        btnOkIconClass: 'glyphicon',
        btnCancelClass: 'btn btn-danger',
        btnCancelIconClass: 'glyphicon'
    })
    .on('confirmed.bs.confirmation', function (e) {
        var testId = '#' + e.target.dataset.testId;
        $(testId + ' label').removeClass('active');
        $(testId + ' label input').prop('checked', '');
        $(e.target).off();

        // ###ORDERING###: update ordering markup
        $(".selectedTest ul#sortableTestBlock").find('li'+testId).remove();
        if(!$(".selectedTest ul#sortableTestBlock li").length){
            $(".selectedTest div.noDataMsg").show();
        }
        update_sort_order();
    });

    // ###ORDERING###: jQuery UI sortable
    $("#sortableTestBlock").sortable({
        items: "li",
        // axis: "x",
        cursor: "move",
        placeholder: "ui-state-highlight",
        start: function(e, ui){

        },
        update: function(e, ui){
            var order = $(this).sortable('toArray',{ attribute: "data-id" });
            order = JSON.stringify(order)
            $(".selectedTest input[type=hidden][name=ordering]").val(order);
            //$('.selectedTest').sortable('disable');
            //$('.selectedTest').sortable('enable');
        },
        stop: function () {

        }
    });

});