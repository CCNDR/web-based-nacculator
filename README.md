Nacculator with Docker and Django
=================================

This tool moves the nacculator tool located at: https://github.com/ctsit/nacculator to a Docker Container that allows the nacculator commands to run using a simple web based interface.  The tool can be run locally on a Windows 10/Mac/Linux enviroment running docker or can be implemented as a public facing web tool with a docker compatible web host.  

This webbased tool allows a user to upload redcap CSV exports and convert them using the Nacculator scripting.  The resulting conversion files along with the conversion log are compressed to a single zip file and downloaded to the user.

**All files created with this system are deleted immeditly after download has completed**

_Note:_ Nacculator with Docker and Django _**requires Docker or Docker Desktop.**_

Setup Instructions:
---------------------

Make Sure Docker or Docker Desktop is installed and running on the machine.
https://www.docker.com/products/docker-desktop

Clone the files to the machine

**Run the following comand inside the cloned directory via command line terminal:**

	$ docker-compose up -d

This will initiate the downloading of the necessary components to build the Docker instance.  Once the process has been completed the Naculator Docker instance should be running.

Upgrade instructions:
------------------------------------

Run the following commands to insure updates are downloaded functioning.

Navigate to the installation folder and run the following using command line:

	$ git pull

	$ docker-compose build

	$ docker-compose down -v

	$ docker-compose up -d

Refresh your installation in the browser and test.

Navigate with any browser to the following URL:
-----------------------------------------------  

	http://localhost:8030

Hosting on domain or subfolder using docker
-----------------------------------------

The script is set to run on the root of a domain.  If you wish to run the script in a subfolder of a domain then edit the .env file refference:

DOMAIN_SUB_FOLDER=''

Adding text here will alter the script to use the subfolder of your choosing.

_Note:_ Place the folder name only, no slashes at the beginning or end.

Configure Field Filter
------------------------------------

The "filter.txt" file in the root allow you to set a standard set of filers for all users.  The custom option allows the user to upload a custom filter file.  It must be a file with a .txt extension with a header of [filter_fix_headers] and the variable names.  ImportFileVariableName: CorrectedVarialeName

**Example text file data:**
[filter_fix_headers]
c1s_2a_npsylan: c1s_2_npsycloc
c1s_2a_npsylanx: c1s_2a_npsylan
b6s_2a1_npsylanx: c1s_2a1_npsylanx
otherneur: othneur
otherneurx: othneurx
strokedec: strokdec
fu_otherneur: fu_othneur
fu_otherneurx: fu_othneurx
fu_strokedec: fu_strokdec
fukid9agd: fu_kid9agd
fusib17pdx: fu_sib17pdx
tele_strokedec: tele_strokdec
telekid9agd: tele_kid9agd
telesib17pdx: tele_sib17pdx
tele_resphear___1: tele_resphear
tele_respdist___1: tele_respdist
tele_respintr___1: tele_respintr
tele_respdisn___1: tele_respdisn
tele_respfatg___1: tele_respfatg
tele_respemot___1: tele_respemot
tele_respasst___1: tele_respasst
tele_respoth___1: tele_respoth

Alternate Command line uses
------------------------------------

The nacculator commands can be run via command line to access features and settings not yet covered in the web tool.

	$ docker exec -t nacc redcap2nacc [COMMANDS FOR NACCULATOR]

**Example basic command:** 

	$ docker exec -t nacc redcap2nacc -fvp -file data.csv >data.txt

**Example - Process a Neuropathology form:**

	$ docker exec -t nacc redcap2nacc -np -file data.csv >data.txt

_Note:_ Files need to be located in the same folder as the script root to properly implement these commands.

Get all Nacculator commands at: https://github.com/ctsit/nacculator